package com.jlngls.caixadetexto;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {



    private TextView resultado;
    private TextInputEditText campoNome, campoEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultado           = findViewById(R.id.resultadoID);
        campoNome           = findViewById(R.id.nnomeID);
        campoEmail          = findViewById(R.id.eemailiD);


    }
    public void onclick(View view) {

        String nome = campoNome.getText().toString();
        String email = campoEmail.getText().toString();
        resultado.setText("Nome: " + nome + "\n" + "Email: " + email);
    }
    public void limparClick(View view){
        campoEmail.setText("");
        campoNome.setText("");
    }
}
